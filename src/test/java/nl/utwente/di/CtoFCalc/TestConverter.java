package nl.utwente.di.CtoFCalc;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class TestConverter {


    @Test
    public void testBook1() throws Exception{
        Converter converter = new Converter();
        double price = converter.convertToF("30");
        Assertions.assertEquals(86, price);
    }
}