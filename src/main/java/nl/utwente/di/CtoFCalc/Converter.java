package nl.utwente.di.CtoFCalc;

public class Converter {

    public Converter() {
    }

    public double convertToF(String input){
        return Double.parseDouble(input) * 1.8 + 32;
    }
}
